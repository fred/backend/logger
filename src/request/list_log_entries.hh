/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIST_LOG_ENTRIES_HH_E7912357A9A243039CEF75F7894363A8
#define LIST_LOG_ENTRIES_HH_E7912357A9A243039CEF75F7894363A8

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"

#include "liblogger/list_log_entries.hh"

#include <boost/optional.hpp>

#include <map>
#include <vector>

namespace Fred {
namespace Logger {

struct ListLogEntriesRequest
{
    LibLogger::LogEntry::ServiceName service;
    LibLogger::LogEntry::TimeBegin from;
    LibLogger::LogEntry::TimeEnd to;
    unsigned int count_limit;
    LibStrong::Optional<LibLogger::LogEntry::UserName> username;
    LibStrong::Optional<LibLogger::LogEntry::UserId> user_id;
    boost::optional<LibLogger::ObjectReference> object_reference;
    std::vector<LibLogger::LogEntry::LogEntryTypeName> log_entry_types;
    std::map<LibLogger::LogEntry::PropertyType, std::vector<LibLogger::LogEntry::PropertyValue>> properties;
};

} // namespace Fred::Logger
} // namespace Fred

#endif
