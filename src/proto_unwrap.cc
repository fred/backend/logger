/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/proto_unwrap.hh"

#include "src/exceptions.hh"

#include "liblogger/exceptions.hh"
#include "liblogger/log_entry.hh"

#include "liblog/liblog.hh"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <google/protobuf/util/time_util.h>

#include <string>
#include <vector>

namespace Fred {
namespace Logger {

namespace {

boost::posix_time::ptime proto_unwrap(const google::protobuf::Timestamp& _src)
{
    const boost::posix_time::ptime timestamp(boost::gregorian::date(1970, 1, 1),
           (boost::posix_time::seconds(_src.seconds()) +
           boost::posix_time::microseconds(_src.nanos()/1000)));
    return timestamp;
}

LibLogger::ObjectReference proto_unwrap(const Api::ObjectReference& _src)
{
    LibLogger::ObjectReference object_reference;
    if (_src.has_type())
    {
        object_reference.object_type = LibLogger::LogEntry::LogEntryObjectTypeName{_src.type().value()};
    }
    if (_src.has_value())
    {
        object_reference.object_ident = LibLogger::LogEntry::ObjectIdent{_src.value().value()};
    }
    return object_reference;
}

LibLogger::LogEntryIdent proto_unwrap(const Api::LogEntryId& _src)
{
    std::vector<std::string> parts;
    boost::split(parts, _src.value(), boost::is_any_of("."));
    if (parts.size() != 4)
    {
        LIBLOG_WARNING("Invalid format: log_entry_ident = {}", _src.value());
        throw LibLogger::InvalidLogEntryIdent{};
    }

    const auto id = [&]()
    {
        try
        {
            return LibLogger::LogEntry::LogEntryId{boost::lexical_cast<unsigned long long>(parts.at(0))};
        }
        catch (const boost::bad_lexical_cast&)
        {
            LIBLOG_WARNING("Could not parse log_entry_id: log_entry_ident = {}", _src.value());
            throw LibLogger::InvalidLogEntryIdent{};
        }
    }();

    const auto time_begin = [&]()
    {
        try
        {
            return LibLogger::LogEntry::TimeBeginDate{boost::gregorian::date_from_iso_string(parts.at(1))};
        }
        catch (const std::exception&)
        {
            LIBLOG_WARNING("Could not parse time_begin: log_entry_ident = {}", _src.value());
            throw LibLogger::InvalidLogEntryIdent{};
        }
    }();

    const auto service_name = LibLogger::LogEntry::ServiceName{parts.at(2)};

    const auto monitoring_status = [&]()
    {
        try
        {
            return LibLogger::LogEntry::IsMonitoring{boost::lexical_cast<bool>(parts.at(3))};
        }
        catch (const boost::bad_lexical_cast&)
        {
            LIBLOG_INFO("Could not parse monitoring_status: log_entry_ident = {}", _src.value());
            throw LibLogger::InvalidLogEntryIdent{};
        }
    }();

    return LibLogger::LogEntryIdent{id, time_begin, service_name, monitoring_status};
}

LibLogger::SessionIdent proto_unwrap(const Api::SessionId& _src)
{
    std::vector<std::string> parts;
    boost::split(parts, _src.value(), boost::is_any_of("."));
    if (parts.size() != 2)
    {
        LIBLOG_WARNING("Invalid format: session_ident = {}", _src.value());
        throw LibLogger::InvalidSessionIdent{};
    }

    auto id = [&]()
    {
        try
        {
            return LibLogger::LogEntry::SessionId{boost::lexical_cast<unsigned long long>(parts.at(0))};
        }
        catch (const LibLogger::InvalidSessionIdent&)
        {
            LIBLOG_WARNING("Could not parse log_entry_id: session_ident = {}", _src.value());
            throw LibLogger::InvalidSessionIdent{};
        }
        catch (const boost::bad_lexical_cast&)
        {
            LIBLOG_WARNING("Could not parse log_entry_id: session_ident = {}", _src.value());
            throw LibLogger::InvalidSessionIdent{};
        }
    }();
    auto login_date = [&]()
    {
        try
        {
            return LibLogger::LogEntry::SessionLoginDate{boost::gregorian::date_from_iso_string(parts.at(1))};
        }
        catch (const std::exception&)
        {
            LIBLOG_WARNING("Could not parse login_date: session_ident = {}", _src.value());
            throw LibLogger::InvalidSessionIdent{};
        }
    }();
    return LibLogger::SessionIdent{std::move(id), std::move(login_date)};
}

std::vector<LibLogger::LogEntryPropertyValue> proto_unwrap(const Api::LogEntryPropertyValues& _src)
{
    std::vector<LibLogger::LogEntryPropertyValue> properties;
    properties.reserve(_src.values_size());
    for (int property_idx = 0; property_idx < _src.values_size(); ++property_idx)
    {
        const auto property_value = _src.values(property_idx).value();
        std::map<LibLogger::LogEntry::PropertyType, std::vector<LibLogger::LogEntry::PropertyValue>> property_children;
        for (const auto& property_child : _src.values(property_idx).children())
        {
            property_children[LibLogger::LogEntry::PropertyType{property_child.first}].push_back(LibLogger::LogEntry::PropertyValue{property_child.second});
        }
        properties.emplace_back(LibLogger::LogEntry::PropertyValue{property_value}, property_children);
    }
    return properties;
}

std::vector<LibLogger::LogEntry::ObjectIdent> proto_unwrap(const Api::ObjectReferenceValues& _src)
{
    std::vector<LibLogger::LogEntry::ObjectIdent> object_references;
    object_references.reserve(_src.references_size());
    for (int object_reference_idx = 0; object_reference_idx < _src.references_size(); ++object_reference_idx)
    {
        const auto object_reference_value = _src.references(object_reference_idx).value();
        object_references.push_back(LibLogger::LogEntry::ObjectIdent{object_reference_value});
    }
    return object_references;
}

} // namespace Fred::Logger::{anonymous}

CreateLogEntryRequest proto_unwrap(const Api::CreateLogEntryRequest& _src)
{
    try
    {
        const auto ip = LibStrong::make_optional(
                _src.has_source_ip(),
                static_cast<std::function<LibLogger::LogEntry::SourceIp()>>(
                        [&]()
                        {
                            boost::system::error_code boost_error_code;
                            auto ip =  boost::asio::ip::address::from_string(_src.source_ip().value(), boost_error_code);
                            if (boost_error_code)
                            {
                                throw InvalidIpAddress{};
                            }
                            return LibLogger::LogEntry::SourceIp{ip};
                        }));

        const auto service =
                _src.has_service()
                        ? LibLogger::LogEntry::ServiceName{_src.service().value()}
                        : throw RequiredArgumentMissing{};

        const auto content = LibStrong::make_optional(
                _src.has_content(),
                static_cast<std::function<LibLogger::LogEntry::Content()>>(
                        [&]() { return LibLogger::LogEntry::Content{_src.content().value()}; }));

        const auto log_entry_properties =
                [&]()
                {
                    std::vector<LibLogger::LogEntryProperty> log_entry_properties;
                    log_entry_properties.reserve(_src.properties_size());
                    for (const auto& properties_of_type : _src.properties())
                    {
                        log_entry_properties.emplace_back(
                                LibLogger::LogEntry::PropertyType{properties_of_type.first},
                                proto_unwrap(properties_of_type.second));
                    }
                    return log_entry_properties;
                }();

        const auto object_references =
                [&]() {
                    std::vector<LibLogger::ObjectReferences> object_references;
                    object_references.reserve(_src.references_size());
                    for (const auto& object_references_of_type : _src.references())
                    {
                        object_references.emplace_back(
                                LibLogger::LogEntry::LogEntryObjectTypeName{object_references_of_type.first},
                                proto_unwrap(object_references_of_type.second));
                    }
                    return object_references;
                }();

        const auto log_entry_type =
                _src.has_log_entry_type()
                        ? LibLogger::LogEntry::LogEntryTypeName{_src.log_entry_type().value()}
                        : throw RequiredArgumentMissing{};

        const auto session_id = [&]() -> boost::optional<LibLogger::SessionIdent>
        {
            if (_src.has_session_id())
            {
                return proto_unwrap(_src.session_id());
            }
            return boost::none;
        };

        return CreateLogEntryRequest{
                ip,
                service,
                content,
                log_entry_properties,
                object_references,
                log_entry_type,
                session_id()};
    }
    catch (const RequiredArgumentMissing& e)
    {
        LIBLOG_INFO("Unwrap Api::CreateLogEntryRequest: {}", e.what());
        throw;
    }
    catch (const InvalidIpAddress& e)
    {
        LIBLOG_INFO("Unwrap Api::CreateLogEntryRequest: {}", e.what());
        throw;
    }
    catch (const LibLogger::InvalidLogEntryIdent& e)
    {
        LIBLOG_INFO("Unwrap Api::CreateLogEntryRequest: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::CreateLogEntryRequest: {}", e.what());
        throw;
    }
}

CloseLogEntryRequest proto_unwrap(const Api::CloseLogEntryRequest& _src)
{
    try
    {
        const auto log_entry_ident = [&]()
        {
            return _src.has_log_entry_id() ? proto_unwrap(_src.log_entry_id()) : throw RequiredArgumentMissing{};
        }();
        const auto log_entry_content = LibStrong::make_optional(
                _src.has_content(),
                static_cast<std::function<LibLogger::LogEntry::Content()>>([&]()
                {
                    return LibLogger::LogEntry::Content{_src.content().value()};
                }));
        const auto log_entry_properties = [&]()
        {
            std::vector<LibLogger::LogEntryProperty> log_entry_properties;
            log_entry_properties.reserve(_src.properties_size());
            for (const auto& properties_of_type : _src.properties())
            {
                log_entry_properties.emplace_back(LibLogger::LogEntry::PropertyType{properties_of_type.first}, proto_unwrap(properties_of_type.second));
            }
            return log_entry_properties;
        }();
        const auto object_references = [&]()
        {
            std::vector<LibLogger::ObjectReferences> object_references;
            object_references.reserve(_src.references_size());
            for (const auto& object_references_of_type : _src.references())
            {
                object_references.emplace_back(LibLogger::LogEntry::LogEntryObjectTypeName{object_references_of_type.first}, proto_unwrap(object_references_of_type.second));
            }
            return object_references;
        }();
        const auto result_code_or_name = [&]() -> boost::variant<LibLogger::LogEntry::ResultCode, LibLogger::LogEntry::ResultName>
        {
            switch (_src.ResultCodeOrName_case())
            {
                case Api::CloseLogEntryRequest::kResultCode:
                    return LibLogger::LogEntry::ResultCode{_src.result_code()};
                case Api::CloseLogEntryRequest::kResultName:
                    return LibLogger::LogEntry::ResultName{_src.result_name()};
                case Api::CloseLogEntryRequest::RESULTCODEORNAME_NOT_SET:
                    throw RequiredArgumentMissing{};
            }
            throw RequiredArgumentMissing{};
        }();
        const auto session_ident = [&]()
        {
            return _src.has_session_id() ? boost::optional<LibLogger::SessionIdent>{proto_unwrap(_src.session_id())} : boost::none;
        }();
        return CloseLogEntryRequest{
                log_entry_ident,
                log_entry_content,
                log_entry_properties,
                object_references,
                result_code_or_name,
                session_ident};
    }
    catch (const RequiredArgumentMissing& e)
    {
        LIBLOG_INFO("Unwrap Api::CloseLogEntryRequest: {}", e.what());
        throw;
    }
    catch (const LibLogger::InvalidLogEntryIdent& e)
    {
        LIBLOG_INFO("Unwrap Api::CloseLogEntryRequest: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::CloseLogEntryRequest: {}", e.what());
        throw;
    }
}

CreateSessionRequest proto_unwrap(const Api::CreateSessionRequest& _src)
{
    CreateSessionRequest request;
    try
    {
        if (_src.has_username())
        {
            request.username = LibLogger::LogEntry::UserName{_src.username().value()};
        }
        else
        {
            throw RequiredArgumentMissing{};
        }
        if (_src.has_user_id())
        {
            request.user_id = LibLogger::LogEntry::UserId{_src.user_id().value()};
        }
    }
    catch (const RequiredArgumentMissing& e)
    {
        LIBLOG_INFO("Unwrap Api::CreateSessionRequest: {}", e.what());
        throw;
    }
    catch (const LibLogger::InvalidUserName& e)
    {
        LIBLOG_INFO("Unwrap Api::CreateSessionRequest: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::CreateSessionRequest: {}", e.what());
        throw;
    }
    return request;
}

CloseSessionRequest proto_unwrap(const Api::CloseSessionRequest& _src)
{
    try
    {
        return CloseSessionRequest{
            _src.has_session_id() ? proto_unwrap(_src.session_id()) : throw RequiredArgumentMissing{}};
    }
    catch (const LibLogger::InvalidSessionIdent& e)
    {
        LIBLOG_INFO("Unwrap Api::CloseSessionRequest: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::CloseSessionRequest: {}", e.what());
        throw;
    }
}

GetLogEntryTypesRequest proto_unwrap(const Api::GetLogEntryTypesRequest& _src)
{
    GetLogEntryTypesRequest request;
    try
    {
        if (_src.has_service())
        {
            request.log_entry_service = LibLogger::LogEntry::ServiceName{_src.service().value()};
        }
        else
        {
            throw RequiredArgumentMissing{};
        }
    }
    catch (const RequiredArgumentMissing& e)
    {
        LIBLOG_INFO("Unwrap Api::GetLogEntryTypesRequest: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::GetLogEntryTypesRequest: {}", e.what());
        throw;
    }
    return request;
}

GetResultsRequest proto_unwrap(const Api::GetResultsRequest& _src)
{
    GetResultsRequest request;
    try
    {
        if (_src.has_service())
        {
            request.log_entry_service = LibLogger::LogEntry::ServiceName{_src.service().value()};
        }
        else
        {
            throw RequiredArgumentMissing{};
        }
    }
    catch (const RequiredArgumentMissing& e)
    {
        LIBLOG_INFO("Unwrap Api::GetResultsRequest: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::GetResultsRequest: {}", e.what());
        throw;
    }
    return request;
}

RegisterServiceRequest proto_unwrap(const Api::RegisterServiceRequest& _src)
{
    RegisterServiceRequest request;
    try
    {
        if (_src.has_service_name())
        {
            request.service_name = LibLogger::LogEntry::ServiceName{_src.service_name().value()};
        }
        else
        {
            throw RequiredArgumentMissing{};
        }
        request.service_handle = LibLogger::LogEntry::ServiceHandle{_src.service_handle()};
    }
    catch (const RequiredArgumentMissing& e)
    {
        LIBLOG_INFO("Unwrap Api::RegisterServiceRequest: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::RegisterServiceRequest: {}", e.what());
        throw;
    }
    return request;
}

RegisterResultsRequest proto_unwrap(const Api::RegisterResultsRequest& _src)
{
    RegisterResultsRequest request;
    try
    {
        if (_src.has_service())
        {
            request.service = LibLogger::LogEntry::ServiceName{_src.service().value()};
        }
        else
        {
            throw RequiredArgumentMissing{};
        }
        if (_src.results_size() == 0)
        {
            throw RequiredArgumentMissing{};
        }
        request.results.reserve(_src.results_size());
        for (const auto& result : _src.results())
        {
            request.results.push_back(
                    LibLogger::Result{
                            LibLogger::LogEntry::ResultCode{result.code()},
                            LibLogger::LogEntry::ResultName{result.name()}});
        }
    }
    catch (const RequiredArgumentMissing& e)
    {
        LIBLOG_INFO("Unwrap Api::RegisterResultsRequest: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::RegisterResultsRequest: {}", e.what());
        throw;
    }
    return request;
}

RegisterLogEntryTypesRequest proto_unwrap(const Api::RegisterLogEntryTypesRequest& _src)
{
    RegisterLogEntryTypesRequest request;
    try
    {
        if (_src.has_service())
        {
            request.service = LibLogger::LogEntry::ServiceName{_src.service().value()};
        }
        else
        {
            throw RequiredArgumentMissing{};
        }
        if (_src.log_entry_types_size() == 0)
        {
            throw RequiredArgumentMissing{};
        }
        request.log_entry_types.reserve(_src.log_entry_types_size());
        for (const auto& log_entry_type : _src.log_entry_types())
        {
            request.log_entry_types.push_back(LibLogger::LogEntry::LogEntryTypeName{log_entry_type.value()});
        }
    }
    catch (const RequiredArgumentMissing& e)
    {
        LIBLOG_INFO("Unwrap Api::RegisterLogEntryTypesRequest: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::RegisterLogEntryTypesRequest: {}", e.what());
        throw;
    }
    return request;
}

RegisterObjectReferenceTypesRequest proto_unwrap(const Api::RegisterObjectReferenceTypesRequest& _src)
{
    RegisterObjectReferenceTypesRequest request;
    try
    {
        request.object_reference_types.reserve(_src.object_types_size());
        for (const auto& object_reference_type : _src.object_types())
        {
            request.object_reference_types.push_back(LibLogger::LogEntry::LogEntryObjectTypeName{object_reference_type.value()});
        }
    }
    catch (const RequiredArgumentMissing& e)
    {
        LIBLOG_INFO("Unwrap Api::RegisterObjectReferenceTypesRequest: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::RegisterObjectReferenceTypesRequest: {}", e.what());
        throw;
    }
    return request;
}

ListLogEntriesRequest proto_unwrap(const Api::ListLogEntriesRequest& _src)
{
    try
    {
        return ListLogEntriesRequest{
                _src.has_service()
                        ? LibLogger::LogEntry::ServiceName{_src.service().value()}
                        : throw RequiredArgumentMissing{},
                _src.has_timestamp_from()
                        ? LibLogger::LogEntry::TimeBegin{proto_unwrap(_src.timestamp_from())}
                        : throw RequiredArgumentMissing{},
                _src.has_timestamp_to()
                        ? LibLogger::LogEntry::TimeEnd{proto_unwrap(_src.timestamp_to())}
                        : throw RequiredArgumentMissing{},
                static_cast<unsigned int>(_src.count_limit()),
                LibStrong::make_optional(
                        _src.has_username(),
                        static_cast<std::function<LibLogger::LogEntry::UserName()>>(
                                [&]()
                                {
                                    return LibLogger::LogEntry::UserName{_src.username().value()};
                                })),
                LibStrong::make_optional(
                        _src.has_user_id(),
                        static_cast<std::function<LibLogger::LogEntry::UserId()>>(
                                [&]()
                                {
                                    return LibLogger::LogEntry::UserId{_src.user_id().value()};
                                })),
                _src.has_reference()
                        ? boost::optional<LibLogger::ObjectReference>{proto_unwrap(_src.reference())}
                        : boost::none,
                [&]()
                {
                    std::vector<LibLogger::LogEntry::LogEntryTypeName> log_entry_types;
                    log_entry_types.reserve(_src.log_entry_types_size());
                    for (const auto& type : _src.log_entry_types())
                    {
                        log_entry_types.push_back(LibLogger::LogEntry::LogEntryTypeName{type.value()});
                    }
                    return log_entry_types;
                }(),
                [&]()
                {
                    std::map<LibLogger::LogEntry::PropertyType, std::vector<LibLogger::LogEntry::PropertyValue>> properties;
                    for (const auto& property : _src.properties())
                    {
                        properties[LibLogger::LogEntry::PropertyType{property.name()}].push_back(LibLogger::LogEntry::PropertyValue{property.value()});
                    }
                    return properties;
                }()
        };
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::ListLogEntriesRequest: {}", e.what());
        throw;
    }
}

GetLogEntryInfoRequest proto_unwrap(const Api::GetLogEntryInfoRequest& _src)
{
    try
    {
        return GetLogEntryInfoRequest{
            _src.has_log_entry_id() ? proto_unwrap(_src.log_entry_id()) : throw RequiredArgumentMissing{}};
    }
    catch (const RequiredArgumentMissing& e)
    {
        LIBLOG_INFO("Unwrap Api::GetLogEntryInfoRequest: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Unwrap Api::GetLogEntryInfoRequest: {}", e.what());
        throw;
    }
}

} // namespace Fred::Logger
} // namespace Fred
