/*
 * Copyright (C) 2020-2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "src/diagnostics/about.hh"

#include "fred_api/logger/service_logger_grpc.grpc.pb.h"
#include "fred_api/logger/service_search_logger_history_grpc.grpc.pb.h"

namespace Fred {
namespace Logger {
namespace Diagnostics {

LibDiagnostics::AboutReply about()
{
    return {
            Fred::LibDiagnostics::AboutReply::ServerVersion{PACKAGE_VERSION},
            {
                    {Api::Logger::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_LOGGER_VERSION}},
                    {Api::SearchLoggerHistory::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_LOGGER_VERSION}}
            }};
}

} // namespace Fred::Logger::Diagnostics
} // namespace Fred::Logger
} // namespace Fred
