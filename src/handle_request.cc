/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/handle_request.hh"

#include "src/get_dsn.hh"
#include "src/is_monitoring_ip.hh"

#include "libpg/pg_ro_transaction.hh"
#include "libpg/pg_rw_transaction.hh"

namespace Fred {
namespace Logger {

LibLogger::CreateLogEntryReply handle_request(const CreateLogEntryRequest& _request)
{
    LibPg::PgRwTransaction rw_transaction{get_dsn()};

    const auto monitoring_status =
            _request.ip != LibLogger::LogEntry::SourceIp::nullopt
                    ? LibLogger::LogEntry::IsMonitoring{is_monitoring_ip(**_request.ip)}
                    : LibLogger::LogEntry::IsMonitoring{false};
    auto result =
            LibLogger::create_log_entry(
                    rw_transaction,
                    _request.ip,
                    monitoring_status,
                    _request.log_entry_service,
                    _request.log_entry_content,
                    _request.log_entry_properties,
                    _request.object_references,
                    _request.log_entry_type,
                    _request.session_ident);
    commit(std::move(rw_transaction));
    return result;
}

void handle_request(const CloseLogEntryRequest& _request)
{
    LibPg::PgRwTransaction rw_transaction{get_dsn()};
    close_log_entry(
            rw_transaction,
            _request.log_entry_ident,
            _request.log_entry_content,
            _request.log_entry_properties,
            _request.object_references,
            _request.result_code_or_name,
            _request.session_ident);
    commit(std::move(rw_transaction));
}

LibLogger::CreateSessionReply handle_request(const CreateSessionRequest& _request)
{
    LibPg::PgRwTransaction rw_transaction{get_dsn()};
    auto result =
            LibLogger::create_session(
                    rw_transaction,
                    _request.username,
                    _request.user_id);
    commit(std::move(rw_transaction));
    return result;
}

void handle_request(const CloseSessionRequest& _request)
{
    LibPg::PgRwTransaction rw_transaction{get_dsn()};
    close_session(
            rw_transaction,
            _request.session_ident);
    commit(std::move(rw_transaction));
}

LibLogger::GetLogEntryTypesReply handle_request(const GetLogEntryTypesRequest& _request)
{
    LibPg::PgRoTransaction ro_transaction{get_dsn()};
    auto result =
            LibLogger::get_log_entry_types(
                    ro_transaction,
                    _request.log_entry_service);
    commit(std::move(ro_transaction));
    return result;
}

LibLogger::GetServicesReply handle_request(const GetServicesRequest&)
{
    LibPg::PgRoTransaction ro_transaction{get_dsn()};
    auto result =
            LibLogger::get_services(
                    ro_transaction);
    commit(std::move(ro_transaction));
    return result;
}

LibLogger::GetResultsReply handle_request(const GetResultsRequest& _request)
{
    LibPg::PgRoTransaction ro_transaction{get_dsn()};
    auto result =
            LibLogger::get_results(
                    ro_transaction,
                    _request.log_entry_service);
    commit(std::move(ro_transaction));
    return result;
}

LibLogger::GetObjectReferenceTypesReply handle_request(const GetObjectReferenceTypesRequest&)
{
    LibPg::PgRoTransaction ro_transaction{get_dsn()};
    auto result =
            LibLogger::get_object_reference_types(
                    ro_transaction);
    commit(std::move(ro_transaction));
    return result;
}

void handle_request(const RegisterServiceRequest& _request)
{
    LibPg::PgRwTransaction rw_transaction{get_dsn()};
    LibLogger::register_service(
            rw_transaction,
            _request.service_name,
            _request.service_handle);
    commit(std::move(rw_transaction));
}

void handle_request(const RegisterResultsRequest& _request)
{
    LibPg::PgRwTransaction rw_transaction{get_dsn()};
    LibLogger::register_results(
            rw_transaction,
            _request.service,
            _request.results);
    commit(std::move(rw_transaction));
}

void handle_request(const RegisterLogEntryTypesRequest& _request)
{
    LibPg::PgRwTransaction rw_transaction{get_dsn()};
    LibLogger::register_log_entry_types(
            rw_transaction,
            _request.service,
            _request.log_entry_types);
    commit(std::move(rw_transaction));
}

void handle_request(const RegisterObjectReferenceTypesRequest& _request)
{
    LibPg::PgRwTransaction rw_transaction{get_dsn()};
    LibLogger::register_object_reference_types(
            rw_transaction,
            _request.object_reference_types);
    commit(std::move(rw_transaction));
}

LibLogger::GetLogEntryInfoReply handle_request(const GetLogEntryInfoRequest& _request)
{
    LibPg::PgRoTransaction ro_transaction{get_dsn()};
    auto result =
            LibLogger::get_log_entry_info(
                    ro_transaction,
                    _request.log_entry_ident);
    commit(std::move(ro_transaction));
    return result;
}

LibLogger::ListLogEntriesReply handle_request(const ListLogEntriesRequest& _request)
{
    LibPg::PgRoTransaction ro_transaction{get_dsn()};
    auto result =
            LibLogger::list_log_entries(
                    ro_transaction,
                    _request.service,
                    _request.from,
                    _request.to,
                    _request.count_limit,
                    _request.username,
                    _request.user_id,
                    _request.object_reference,
                    _request.log_entry_types,
                    _request.properties);
    commit(std::move(ro_transaction));
    return result;
}

} // namespace Fred::Logger
} // namespace Fred
