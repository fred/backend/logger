/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_RESULTS_HH_FC684C9BE4764FF882AD7D9B585AA251
#define GET_RESULTS_HH_FC684C9BE4764FF882AD7D9B585AA251

#include "liblogger/log_entry.hh"

#include <map>
#include <string>

namespace Fred {
namespace Logger {

struct GetResultsRequest
{
    LibLogger::LogEntry::ServiceName log_entry_service;
};

} // namespace Fred::Logger
} // namespace Fred

#endif
