/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATE_LOG_ENTRY_HH_5C1083754A9E428B92CCCE1B201744D1
#define CREATE_LOG_ENTRY_HH_5C1083754A9E428B92CCCE1B201744D1

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"
#include "liblogger/session_ident.hh"

#include <boost/optional.hpp>

#include <vector>

namespace Fred {
namespace Logger {

struct CreateLogEntryRequest
{
    LibStrong::Optional<LibLogger::LogEntry::SourceIp> ip;
    LibLogger::LogEntry::ServiceName log_entry_service;
    LibStrong::Optional<LibLogger::LogEntry::Content> log_entry_content;
    std::vector<LibLogger::LogEntryProperty> log_entry_properties;
    std::vector<LibLogger::ObjectReferences> object_references;
    LibLogger::LogEntry::LogEntryTypeName log_entry_type;
    boost::optional<LibLogger::SessionIdent> session_ident;
};

} // namespace Fred::Logger
} // namespace Fred

#endif
