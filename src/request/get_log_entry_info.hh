/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_LOG_ENTRY_INFO_HH_5B76890ACCEB40669FCC45E909C645D0
#define GET_LOG_ENTRY_INFO_HH_5B76890ACCEB40669FCC45E909C645D0

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/optional/optional.hpp>

#include <vector>

namespace Fred {
namespace Logger {

struct GetLogEntryInfoRequest
{
    LibLogger::LogEntryIdent log_entry_ident;
};

} // namespace Fred::Logger
} // namespace Fred

#endif
