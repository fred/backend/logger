/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/proto_wrap.hh"

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"
#include "liblogger/session_ident.hh"

#include <boost/algorithm/string.hpp>
#include <boost/asio/ip/address.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <map>
#include <string>
#include <utility>
#include <sstream>

namespace Fred {
namespace Logger {

namespace {

void proto_wrap(const boost::posix_time::ptime& _src, google::protobuf::Timestamp* _dst)
{
    const boost::posix_time::ptime epoch(boost::gregorian::date(1970, boost::date_time::Jan, 1),
            boost::posix_time::time_duration(0, 0, 0));
    const boost::posix_time::time_duration diff = _src - epoch;
    long long total_seconds = diff.total_seconds();
    long long fractional_seconds = diff.total_nanoseconds() % 1'000'000'000;
    if (diff.is_negative())
    {
        total_seconds--;
        fractional_seconds += 1'000'000'000;
    }
    _dst->set_seconds(total_seconds);
    _dst->set_nanos(fractional_seconds);
}

void proto_wrap(const boost::asio::ip::address& _src, Api::SourceIp* _dst)
{
    _dst->set_value(_src.to_string());
}

void proto_wrap(
        const std::vector<LibLogger::LogEntryPropertyValue>& src,
        Api::LogEntryPropertyValues& dst)
{
    dst.clear_values();
    dst.mutable_values()->Reserve(src.size());
    for (const auto& prop : src)
    {
        auto* const dst_prop = dst.add_values();
        dst_prop->set_value(*prop.property_value);
        for (const auto& child : prop.children)
        {
            const auto& child_property_type = child.first;
            auto* const dst_prop_children = dst_prop->mutable_children();
            for (const auto& child_property_value : child.second)
            {
                (*dst_prop_children)[*child_property_type] = *child_property_value;
            }
        }
    }
}

void proto_wrap(
        const LibLogger::LogEntryProperty& _src,
        google::protobuf::Map<std::string, Api::LogEntryPropertyValues>* _dst)
{
    if ((*_dst)[*_src.property_type].values().empty())
    {
        proto_wrap(_src.property_values, (*_dst)[*_src.property_type]);
        return;
    }
    // deprecated case (input/output properties merge)
    for (auto&& prop : _src.property_values)
    {
        const auto& dst_prop = (*_dst)[*_src.property_type].add_values();
        dst_prop->set_value(*prop.property_value);
        auto* const dst_prop_children = dst_prop->mutable_children();
        for (auto&& child : prop.children)
        {
            if (!child.second.empty())
            {
                // lossy vector compression into a scalar
                (*dst_prop_children)[*child.first] = *child.second.back();
            }
        }
    }
}

void proto_wrap(
        const LibLogger::ObjectReferences& _src,
        google::protobuf::Map<std::string, Api::ObjectReferenceValues>* _dst)
{
    Api::ObjectReferenceValues object_refs;
    object_refs.mutable_references()->Reserve(_src.object_reference_ids.size());
    for (const auto& id : _src.object_reference_ids)
    {
        object_refs.add_references()->set_value(*id);
    }
    (*_dst)[*(_src.object_type)] = object_refs;
}

void proto_wrap(
        const std::map<LibLogger::LogEntry::ResultName, LibLogger::LogEntry::ResultCode>& _src,
        google::protobuf::Map<std::string, google::protobuf::uint64>* _dst)
{
    for (const auto& src_item : _src)
    {
        (*_dst)[*(src_item.first)] = *(src_item.second);
    }
}

void proto_wrap(const LibLogger::LogEntryIdent& src, Api::LogEntryId* dst)
{
    dst->set_value(
            std::to_string(*src.id) + "." +
            boost::gregorian::to_iso_string(*src.time_begin) + "." +
            boost::algorithm::to_lower_copy(*src.service_name) + "." +
            std::to_string(static_cast<int>(*src.monitoring_status)));
}

void proto_wrap(const LibLogger::SessionIdent& src, Api::SessionId* dst)
{
    dst->set_value(
            std::to_string(*src.id) + "." +
            boost::gregorian::to_iso_string(*src.login_date));
}

} // namespace Fred::Logger::{anonymous}

void proto_wrap(const LibLogger::CreateLogEntryReply& _src, Api::CreateLogEntryReply* _dst)
{
    _dst->clear_data();
    proto_wrap(_src.log_entry_ident, _dst->mutable_data()->mutable_log_entry_id());
}

void proto_wrap(const LibLogger::CreateSessionReply& _src, Api::CreateSessionReply* _dst)
{
    _dst->clear_data();
    proto_wrap(_src.session_ident, _dst->mutable_data()->mutable_session_id());
}

void proto_wrap(const LibLogger::GetLogEntryTypesReply& _src, Api::GetLogEntryTypesReply* _dst)
{
    _dst->clear_data();
    auto* const dst_data = _dst->mutable_data();
    dst_data->mutable_types()->Reserve(_src.log_entry_types.size());
    for (const auto& log_entry_type : _src.log_entry_types)
    {
        dst_data->add_types()->set_value(*log_entry_type);
    }
}

void proto_wrap(const LibLogger::GetServicesReply& _src, Api::GetServicesReply* _dst)
{
    _dst->clear_data();
    auto* const dst_data = _dst->mutable_data();
    dst_data->mutable_services()->Reserve(_src.log_entry_services.size());
    for (const auto& log_entry_service : _src.log_entry_services)
    {
        dst_data->add_services()->set_value(*log_entry_service);
    }
}

void proto_wrap(const LibLogger::GetResultsReply& _src, Api::GetResultsReply* _dst)
{
    _dst->clear_data();
    proto_wrap(_src.result_code_map, _dst->mutable_data()->mutable_result_code_map());
}

void proto_wrap(const LibLogger::GetObjectReferenceTypesReply& _src, Api::GetObjectReferenceTypesReply* _dst)
{
    _dst->clear_data();
    auto* const dst_data = _dst->mutable_data();
    dst_data->mutable_object_types()->Reserve(_src.object_reference_types.size());
    for (const auto& object_reference_type : _src.object_reference_types)
    {
        dst_data->add_object_types()->set_value(*object_reference_type);
    }
}

void proto_wrap(const LibLogger::SessionDoesNotExist&, Api::CloseSessionReply* _dst)
{
    _dst->clear_exception();
    _dst->mutable_exception()->mutable_session_does_not_exist();
}

void proto_wrap(const LibLogger::LogEntryDoesNotExist&, Api::CloseLogEntryReply* _dst)
{
    _dst->clear_exception();
    _dst->mutable_exception()->mutable_log_entry_does_not_exist();
}

void proto_wrap(const LibLogger::ListLogEntriesReply& _src, Api::ListLogEntriesReply* _dst)
{
    _dst->clear_data();
    auto* const dst_data = _dst->mutable_data();
    dst_data->mutable_log_entry_ids()->Reserve(_src.log_entry_ids.size());
    for (const auto& ident : _src.log_entry_ids)
    {
        proto_wrap(ident, dst_data->add_log_entry_ids());
    }
}

void proto_wrap(const LibLogger::ServiceNotFound&, Api::ListLogEntriesReply* _dst)
{
    _dst->clear_exception();
    _dst->mutable_exception()->mutable_log_entry_service_not_found();
}

void proto_wrap(const LibLogger::InvalidTimestampFrom&, Api::ListLogEntriesReply* _dst)
{
    _dst->clear_exception();
    _dst->mutable_exception()->mutable_invalid_timestamp_from();
}

void proto_wrap(const LibLogger::InvalidTimestampTo&, Api::ListLogEntriesReply* _dst)
{
    _dst->clear_exception();
    _dst->mutable_exception()->mutable_invalid_timestamp_to();
}

void proto_wrap(const LibLogger::InvalidTimestampRange&, Api::ListLogEntriesReply* _dst)
{
    _dst->clear_exception();
    _dst->mutable_exception()->mutable_invalid_timestamp_range();
}

void proto_wrap(const LibLogger::InvalidCountLimit&, Api::ListLogEntriesReply* _dst)
{
    _dst->clear_exception();
    _dst->mutable_exception()->mutable_invalid_count_limit();
}

void proto_wrap(const LibLogger::GetLogEntryInfoReply& _src, Api::GetLogEntryInfoReply* _dst)
{
    _dst->clear_data();
    Api::LogEntryInfo* const dst_info = _dst->mutable_data()->mutable_log_entry_info();
    const LibLogger::LogEntryInfo& src_info = _src.log_entry_info;

    proto_wrap(*(src_info.time_begin), dst_info->mutable_time_begin());

    dst_info->mutable_service()->set_value(*(src_info.log_entry_ident.service_name));
    if (has_value(src_info.time_end) && !(**(src_info.time_end)).is_special())
    {
        proto_wrap(**(src_info.time_end), dst_info->mutable_time_end());
    }
    if (has_value(src_info.source_ip))
    {
        proto_wrap(**(src_info.source_ip), dst_info->mutable_source_ip());
    }
    if (has_value(src_info.log_entry_type))
    {
        dst_info->mutable_log_entry_type()->set_value(**(src_info.log_entry_type));
    }
    if (has_value(src_info.result_code))
    {
        dst_info->set_result_code(**(src_info.result_code));
    }
    if (has_value(src_info.result_name))
    {
        dst_info->set_result_name(**(src_info.result_name));
    }
    if (has_value(src_info.raw_request))
    {
        dst_info->mutable_raw_request()->set_value(**(src_info.raw_request));
    }
    if (has_value(src_info.raw_response))
    {
        dst_info->mutable_raw_response()->set_value(**(src_info.raw_response));
    }
    if (has_value(src_info.username))
    {
        dst_info->mutable_username()->set_value(**(src_info.username));
    }
    if (has_value(src_info.user_id))
    {
        dst_info->mutable_user_id()->set_value(**(src_info.user_id));
    }
    for (const auto& props : src_info.input_properties)
    {
        proto_wrap(props, dst_info->mutable_input_properties());
        proto_wrap(props, dst_info->mutable_properties()); // deprecated
    }
    for (const auto& props : src_info.output_properties)
    {
        proto_wrap(props, dst_info->mutable_output_properties());
        proto_wrap(props, dst_info->mutable_properties()); // deprecated
    }
    for (const auto& refs : src_info.object_references)
    {
        proto_wrap(refs, dst_info->mutable_references());
    }
    proto_wrap(src_info.log_entry_ident, dst_info->mutable_log_entry_id());
    if (src_info.session_ident != boost::none)
    {
        proto_wrap(*src_info.session_ident, dst_info->mutable_session_id());
    }    
}

void proto_wrap(const LibLogger::LogEntryDoesNotExist&, Api::GetLogEntryInfoReply* _dst)
{
    _dst->clear_data();
    _dst->mutable_exception()->mutable_log_entry_does_not_exist();
}

} // namespace Fred::Logger
} // namespace Fred
