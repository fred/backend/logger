/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_DSN_HH_462D2683A247480E9BE91E0F545685E0
#define GET_DSN_HH_462D2683A247480E9BE91E0F545685E0

#include "libpg/dsn.hh"

namespace Fred {
namespace Logger {

LibPg::Dsn get_dsn();

} // namespace Fred::Logger
} // namespace Fred

#endif
