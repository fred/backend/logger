/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HANDLE_REQUEST_HH_38C3811FBE664B2695388F2D21D51244
#define HANDLE_REQUEST_HH_38C3811FBE664B2695388F2D21D51244

#include "request/register_log_entry_types.hh"
#include "request/register_object_reference_types.hh"
#include "request/register_results.hh"
#include "request/register_service.hh"
#include "request/close_log_entry.hh"
#include "request/close_session.hh"
#include "request/create_log_entry.hh"
#include "request/create_session.hh"
#include "request/get_log_entry_info.hh"
#include "request/get_log_entry_types.hh"
#include "request/get_object_reference_types.hh"
#include "request/get_results.hh"
#include "request/get_services.hh"
#include "request/list_log_entries.hh"

#include "liblogger/register_log_entry_types.hh"
#include "liblogger/register_object_reference_types.hh"
#include "liblogger/register_results.hh"
#include "liblogger/register_service.hh"
#include "liblogger/close_log_entry.hh"
#include "liblogger/close_session.hh"
#include "liblogger/create_log_entry.hh"
#include "liblogger/create_session.hh"
#include "liblogger/get_log_entry_info.hh"
#include "liblogger/get_log_entry_types.hh"
#include "liblogger/get_object_reference_types.hh"
#include "liblogger/get_results.hh"
#include "liblogger/get_services.hh"
#include "liblogger/list_log_entries.hh"

namespace Fred {
namespace Logger {

LibLogger::CreateLogEntryReply handle_request(const CreateLogEntryRequest& _request);

void handle_request(const CloseLogEntryRequest& _request);

LibLogger::CreateSessionReply handle_request(const CreateSessionRequest& _request);

void handle_request(const CloseSessionRequest& _request);

LibLogger::GetLogEntryTypesReply handle_request(const GetLogEntryTypesRequest& _request);

LibLogger::GetServicesReply handle_request(const GetServicesRequest& _request);

LibLogger::GetResultsReply handle_request(const GetResultsRequest& _request);

LibLogger::GetObjectReferenceTypesReply handle_request(const GetObjectReferenceTypesRequest& _request);

void handle_request(const RegisterServiceRequest& _request);

void handle_request(const RegisterResultsRequest& _request);

void handle_request(const RegisterLogEntryTypesRequest& _request);

void handle_request(const RegisterObjectReferenceTypesRequest& _request);

LibLogger::GetLogEntryInfoReply handle_request(const GetLogEntryInfoRequest& _request);

LibLogger::ListLogEntriesReply handle_request(const ListLogEntriesRequest& _request);

} // namespace Fred::Logger
} // namespace Fred

#endif
