/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PROTO_WRAP_HH_666191D801524FF88F3FA9E2523BF0DF
#define PROTO_WRAP_HH_666191D801524FF88F3FA9E2523BF0DF

#include "liblogger/create_log_entry.hh"
#include "liblogger/create_session.hh"
#include "liblogger/exceptions.hh"
#include "liblogger/get_log_entry_info.hh"
#include "liblogger/get_log_entry_types.hh"
#include "liblogger/get_object_reference_types.hh"
#include "liblogger/get_results.hh"
#include "liblogger/get_services.hh"
#include "liblogger/list_log_entries.hh"

#include "fred_api/logger/service_logger_grpc.pb.h"
#include "fred_api/logger/service_search_logger_history_grpc.pb.h"

namespace Fred {
namespace Logger {

void proto_wrap(const LibLogger::CreateLogEntryReply& _src, Api::CreateLogEntryReply* _dst);

void proto_wrap(const LibLogger::CreateSessionReply& _src, Api::CreateSessionReply* _dst);

void proto_wrap(const LibLogger::GetLogEntryTypesReply& _src, Api::GetLogEntryTypesReply* _dst);

void proto_wrap(const LibLogger::GetServicesReply& _src, Api::GetServicesReply* _dst);

void proto_wrap(const LibLogger::GetResultsReply& _src, Api::GetResultsReply* _dst);

void proto_wrap(const LibLogger::GetObjectReferenceTypesReply& _src, Api::GetObjectReferenceTypesReply* _dst);

void proto_wrap(const LibLogger::SessionDoesNotExist& _src, Api::CloseSessionReply* _dst);

void proto_wrap(const LibLogger::LogEntryDoesNotExist& _src, Api::CloseLogEntryReply* _dst);

void proto_wrap(const LibLogger::ListLogEntriesReply& _src, Api::ListLogEntriesReply* _dst);

void proto_wrap(const LibLogger::ServiceNotFound& _src, Api::ListLogEntriesReply* _dst);

void proto_wrap(const LibLogger::InvalidTimestampFrom& _src, Api::ListLogEntriesReply* _dst);

void proto_wrap(const LibLogger::InvalidTimestampTo& _src, Api::ListLogEntriesReply* _dst);

void proto_wrap(const LibLogger::InvalidTimestampRange& _src, Api::ListLogEntriesReply* _dst);

void proto_wrap(const LibLogger::InvalidCountLimit& _src, Api::ListLogEntriesReply* _dst);

void proto_wrap(const LibLogger::GetLogEntryInfoReply& _src, Api::GetLogEntryInfoReply* _dst);

void proto_wrap(const LibLogger::LogEntryDoesNotExist& _src, Api::GetLogEntryInfoReply* _dst);

} // namespace Fred::Logger
} // namespace Fred

#endif
