/*
 * Copyright (C) 2019-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "config.h"

#include "config.h"

#include "src/cfg.hh"
#include "src/diagnostics/about.hh"
#include "src/diagnostics/status.hh"
#include "src/exceptions.hh"
#include "src/handle_request.hh"
#include "src/proto_unwrap.hh"
#include "src/proto_wrap.hh"

#include "fred_api/logger/service_logger_grpc.grpc.pb.h"
#include "fred_api/logger/service_logger_grpc.pb.h"
#include "fred_api/logger/service_search_logger_history_grpc.grpc.pb.h"
#include "fred_api/logger/service_search_logger_history_grpc.pb.h"

#include "libdiagnostics/diagnostics.hh"

#include "liblogger/exceptions.hh"

#include "liblog/context.hh"
#include "liblog/liblog.hh"
#include "liblog/log.hh"
#include "liblog/log_config.hh"
#include "liblog/sink/console_sink_config.hh"
#include "liblog/sink/file_sink_config.hh"
#include "liblog/sink/syslog_sink_config.hh"

#include "libpg/detail/libpq_layer_impl.hh"
#include "libpg/libpq_layer.hh"
#include "libpg/pg_connection.hh"

#include <grpc++/grpc++.h>

#include <csignal>
#include <cstring>
#include <exception>
#include <memory>
#include <sstream>
#include <string>
#include <utility>

#define LOGGER_GRPC_REQUEST_CONTEXT() \
struct RequestContext \
{ \
    LibLog::SetContext ctx; \
    RequestContext(std::initializer_list<std::string> _context_hierarchy) : ctx{_context_hierarchy} \
    { \
        LIBLOG_INFO("begin"); \
    } \
    ~RequestContext() \
    { \
        try \
        { \
            LIBLOG_INFO("end"); \
        } \
        catch (...) { } \
    } \
} request_context{__func__}

namespace Fred {
namespace Logger {
namespace {

class LoggerService final : public Api::Logger::Service
{
    grpc::Status create_log_entry(
            grpc::ServerContext*,
            const Api::CreateLogEntryRequest* _request,
            Api::CreateLogEntryReply* _response) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            const auto result = ::Fred::Logger::handle_request(request);
            proto_wrap(result, _response);
            return grpc::Status::OK;
        }
        catch (const RequiredArgumentMissing& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "required argument missing");
        }
        catch (const InvalidIpAddress& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid ip address");
        }
        catch (const LibLogger::InvalidSessionIdent& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid session ident");
        }
        catch (const LibLogger::InvalidPropertyType& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid property type");
        }
        catch (const LibLogger::InvalidService& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid service");
        }
        catch (const LibLogger::ServiceNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "service not found");
        }
        catch (const LibLogger::LogEntryTypeNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "log entry type not found");
        }
        catch (const LibLogger::ObjectTypeNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "object type not found");
        }
        catch (const LibLogger::SessionDoesNotExist& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "session not found");
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status close_log_entry(
            grpc::ServerContext*,
            const Api::CloseLogEntryRequest* _request,
            Api::CloseLogEntryReply* _response) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            ::Fred::Logger::handle_request(request);
            return grpc::Status::OK;
        }
        catch (const RequiredArgumentMissing& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "required argument missing");
        }
        catch (const LibLogger::InvalidSessionIdent& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid session ident");
        }
        catch (const LibLogger::InvalidLogEntryIdent& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid log entry ident");
        }
        catch (const LibLogger::InvalidPropertyType& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid property type");
        }
        catch (const LibLogger::LogEntryTypeNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "log entry type not found");
        }
        catch (const LibLogger::ServiceNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "service not found");
        }
        catch (const LibLogger::ObjectTypeNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "object type not found");
        }
        catch (const LibLogger::ResultCodeNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "result code not found");
        }
        catch (const LibLogger::SessionDoesNotExist& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "session not found");
        }
        catch (const LibLogger::LogEntryDoesNotExist& e)
        {
            LIBLOG_INFO("{}", e.what());
            proto_wrap(e, _response);
            return grpc::Status::OK;
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status create_session(
            grpc::ServerContext*,
            const Api::CreateSessionRequest* _request,
            Api::CreateSessionReply* _response) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            const auto result = ::Fred::Logger::handle_request(request);
            proto_wrap(result, _response);
            return grpc::Status::OK;
        }
        catch (const RequiredArgumentMissing& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "required argument missing");
        }
        catch (const LibLogger::InvalidUserName& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid user name");
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status close_session(
            grpc::ServerContext*,
            const Api::CloseSessionRequest* _request,
            Api::CloseSessionReply* _response) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            ::Fred::Logger::handle_request(request);
            return grpc::Status::OK;
        }
        catch (const RequiredArgumentMissing& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "required argument missing");
        }
        catch (const LibLogger::InvalidSessionIdent& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid session ident");
        }
        catch (const LibLogger::SessionAlreadyClosed& e)
        {
            LIBLOG_INFO("{}", e.what());
            proto_wrap(LibLogger::SessionDoesNotExist{}, _response);
            return grpc::Status::OK;
        }
        catch (const LibLogger::SessionDoesNotExist& e)
        {
            LIBLOG_INFO("{}", e.what());
            proto_wrap(e, _response);
            return grpc::Status::OK;
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status get_log_entry_types(
            grpc::ServerContext*,
            const Api::GetLogEntryTypesRequest* _request,
            Api::GetLogEntryTypesReply* _response) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            const auto result = ::Fred::Logger::handle_request(request);
            proto_wrap(result, _response);
            return grpc::Status::OK;
        }
        catch (const RequiredArgumentMissing& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "required argument missing");
        }
        catch (const LibLogger::InvalidService& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid service");
        }
        catch (const LibLogger::ServiceNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "service not found");
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status get_services(
            grpc::ServerContext*,
            const ::google::protobuf::Empty*,
            Api::GetServicesReply* _response) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto result = ::Fred::Logger::handle_request(GetServicesRequest{});
            proto_wrap(result, _response);
            return grpc::Status::OK;
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status get_results(
            grpc::ServerContext*,
            const Api::GetResultsRequest* _request,
            Api::GetResultsReply* _response) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            const auto result = ::Fred::Logger::handle_request(request);
            proto_wrap(result, _response);
            return grpc::Status::OK;
        }
        catch (const RequiredArgumentMissing& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "required argument missing");
        }
        catch (const LibLogger::InvalidService& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid service");
        }
        catch (const LibLogger::ServiceNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "service not found");
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status get_object_reference_types(
            grpc::ServerContext*,
            const ::google::protobuf::Empty*,
            Api::GetObjectReferenceTypesReply* _response) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto result = ::Fred::Logger::handle_request(GetObjectReferenceTypesRequest{});
            proto_wrap(result, _response);
            return grpc::Status::OK;
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status register_service(
            grpc::ServerContext*,
            const Api::RegisterServiceRequest* _request,
            ::google::protobuf::Empty*) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            ::Fred::Logger::handle_request(request);
            return grpc::Status::OK;
        }
        catch (const RequiredArgumentMissing& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "required argument missing");
        }
        catch (const LibLogger::InvalidService& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid service");
        }
        catch (const LibLogger::ServiceRegistrationFailed& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "service registration failed");
        }
        catch (const LibLogger::ServiceExists& e)
        {
            return grpc::Status::OK;
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status register_results(
            grpc::ServerContext*,
            const Api::RegisterResultsRequest* _request,
            ::google::protobuf::Empty*) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            ::Fred::Logger::handle_request(request);
            return grpc::Status::OK;
        }
        catch (const RequiredArgumentMissing& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "required argument missing");
        }
        catch (const LibLogger::InvalidService& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid service");
        }
        catch (const LibLogger::ServiceNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "service not found");
        }
        catch (const LibLogger::InvalidResultName& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid result name");
        }
        catch (const LibLogger::InvalidResult& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid result");
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status register_log_entry_types(
            grpc::ServerContext*,
            const Api::RegisterLogEntryTypesRequest* _request,
            ::google::protobuf::Empty*) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            ::Fred::Logger::handle_request(request);
            return grpc::Status::OK;
        }
        catch (const RequiredArgumentMissing& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "required argument missing");
        }
        catch (const LibLogger::InvalidService& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid service");
        }
        catch (const LibLogger::ServiceNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "service not found");
        }
        catch (const LibLogger::InvalidLogEntryType& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid log entry type");
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status register_object_reference_types(
            grpc::ServerContext*,
            const Api::RegisterObjectReferenceTypesRequest* _request,
            ::google::protobuf::Empty*) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            ::Fred::Logger::handle_request(request);
            return grpc::Status::OK;
        }
        catch (const LibLogger::InvalidObjectType& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid object type");
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

};

class SearchLoggerHistoryService final : public Api::SearchLoggerHistory::Service
{
    grpc::Status list_log_entries(
            grpc::ServerContext*,
            const Api::ListLogEntriesRequest* _request,
            Api::ListLogEntriesReply* _response) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            const auto result = ::Fred::Logger::handle_request(request);
            proto_wrap(result, _response);
            return grpc::Status::OK;
        }
        catch (const RequiredArgumentMissing& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "required argument missing");
        }
        catch (const LibLogger::InvalidService& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid service");
        }
        catch (const LibLogger::ServiceNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            proto_wrap(e, _response);
            return grpc::Status::OK;
        }
        catch (const LibLogger::InvalidTimestampFrom& e)
        {
            LIBLOG_INFO("{}", e.what());
            proto_wrap(e, _response);
            return grpc::Status::OK;
            //return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid timestamp from");
        }
        catch (const LibLogger::InvalidTimestampTo& e)
        {
            LIBLOG_INFO("{}", e.what());
            proto_wrap(e, _response);
            return grpc::Status::OK;
            //return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid timestamp to");
        }
        catch (const LibLogger::InvalidTimestampRange& e)
        {
            LIBLOG_INFO("{}", e.what());
            proto_wrap(e, _response);
            return grpc::Status::OK;
            //return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid timestamp range");
        }
        catch (const LibLogger::InvalidCountLimit& e)
        {
            LIBLOG_INFO("{}", e.what());
            proto_wrap(e, _response);
            return grpc::Status::OK;
            //return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "invalid timestamp count limit");
        }
        catch (const LibLogger::ListLogEntriesException& e)
        {
            LIBLOG_WARNING("{}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "failed to list log entries due to an unknown exception");
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }

    grpc::Status get_log_entry_info(
            grpc::ServerContext*,
            const Api::GetLogEntryInfoRequest* _request,
            Api::GetLogEntryInfoReply* _response) override
    {
        LOGGER_GRPC_REQUEST_CONTEXT();
        try
        {
            const auto request = proto_unwrap(*_request);
            const auto result = ::Fred::Logger::handle_request(request);
            proto_wrap(result, _response);
            return grpc::Status::OK;
        }
        catch (const RequiredArgumentMissing& e)
        {
            LIBLOG_INFO("{}", e.what());
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "required argument missing");
        }
        catch (const LibLogger::InvalidLogEntryIdent& e)
        {
            LIBLOG_INFO("{}", e.what());
            proto_wrap(LibLogger::LogEntryDoesNotExist(), _response);
            return grpc::Status::OK;
        }
        catch (const LibLogger::ServiceNotFound& e)
        {
            LIBLOG_INFO("{}", e.what());
            proto_wrap(LibLogger::LogEntryDoesNotExist(), _response);
            return grpc::Status::OK;
        }
        catch (const LibLogger::LogEntryDoesNotExist& e)
        {
            LIBLOG_INFO("{}", e.what());
            proto_wrap(e, _response);
            return grpc::Status::OK;
        }
        catch (const std::exception& e)
        {
            LIBLOG_ERROR("server caught unexpected exception: {}", e.what());
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
        catch (...)
        {
            LIBLOG_ERROR("server caught unexpected exception");
            return grpc::Status(grpc::StatusCode::UNKNOWN, "unexpected exception");
        }
    }
};

enum class LogHandler
{
    is_set,
    is_not_set
};

class SetLogHandler : public boost::static_visitor<LogHandler>
{
public:
    explicit SetLogHandler(LibLog::LogConfig& liblog_log_config)
        : liblog_log_config_{liblog_log_config}
    {
    }
    LogHandler operator()(const boost::blank&) const
    {
        return LogHandler::is_not_set;
    }
    LogHandler operator()(const Cfg::Options::Log::Console& option) const
    {
        auto console_sink_config =
                LibLog::Sink::ConsoleSinkConfig()
                        .set_output_stream(LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr);
        if (option.min_severity != boost::none)
        {
            console_sink_config.set_level(*option.min_severity);
        }
        liblog_log_config_.add_sink_config(console_sink_config);
        return LogHandler::is_set;
    }
    LogHandler operator()(const Cfg::Options::Log::Logfile& option) const
    {
        auto file_sink_config =
                LibLog::Sink::FileSinkConfig(option.file_name);
        if (option.min_severity != boost::none)
        {
            file_sink_config.set_level(*option.min_severity);
        }
        liblog_log_config_.add_sink_config(file_sink_config);
        return LogHandler::is_set;
    }
    LogHandler operator()(const Cfg::Options::Log::Syslog& option) const
    {
        auto syslog_sink_config =
                LibLog::Sink::SyslogSinkConfig()
                        .set_syslog_facility(option.facility);
        if (option.min_severity != boost::none)
        {
            syslog_sink_config.set_level(*option.min_severity);
        }
        liblog_log_config_.add_sink_config(syslog_sink_config);
        return LogHandler::is_set;
    }
private:
    LibLog::LogConfig& liblog_log_config_;
};

void show_query_argument_value(std::ostream& out, int idx, const char* value)
{
    if (idx != 0)
    {
        out << ", ";
    }
    out << "$" << (idx + 1) << ":";
    if (value == nullptr)
    {
        out << "NULL";
    }
    else
    {
        out << "\"" << value << "\"";
    }
}

auto values_to_string(const char* const* values, int number_of_values)
{
    std::ostringstream out;
    out << "[";
    for (int idx = 0; idx < number_of_values; ++idx)
    {
        show_query_argument_value(out, idx, values[idx]);
    }
    out << "]";
    return std::move(out).str();
}

class LibPgLogging
{
public:
    LibPgLogging()
    {
        LibPg::set_libpq_layer(static_cast<LibPg::LibPqLayer*>(&my_libpq_layer_impl_));
    }
    ~LibPgLogging()
    {
        LibPg::set_libpq_layer(nullptr);
    }
private:
    class LibPq : public LibPg::Detail::LibPqLayerSimpleImplementation
    {
    public:
        LibPq()
        {
        }
        ~LibPq() override
        {
        }
    private:
        using Parent = LibPg::Detail::LibPqLayerSimpleImplementation;
        ::PGconn* PQconnectdbParams(
                const char* const* keywords,
                const char* const* values,
                int expand_dbname) const override
        {
            auto* const conn = this->Parent::PQconnectdbParams(keywords, values, expand_dbname);
            this->PQsetNoticeProcessor(
                    conn,
                    [](void*, const char* message) {
                        // notice|warning messages from PostgreSQL
                        LIBLOG_WARNING((std::string{message, std::strlen(message) - 1}));
                    },
                    nullptr);
            return conn;
        }
        ::PGresult* PQexec(::PGconn* conn, const char* query) const override
        {
            LIBLOG_DEBUG("query: {}", query);
            return this->Parent::PQexec(conn, query);
        }
        ::PGresult* PQexecParams(
                ::PGconn* conn,
                const char* command,
                int nParams,
                const ::Oid* paramTypes,
                const char* const* paramValues,
                const int* paramLengths,
                const int* paramFormats,
                int resultFormat) const override
        {
            LIBLOG_DEBUG("query: {} {}", command, values_to_string(paramValues, nParams));
            return this->Parent::PQexecParams(conn, command, nParams, paramTypes, paramValues, paramLengths, paramFormats, resultFormat);
        }
    };
    LibPq my_libpq_layer_impl_;
};

void run_server(const std::string& server_address)
{
    LIBLOG_SET_CONTEXT(Context, ctx, __func__);
    ::sigset_t blocked_signals;

    // Other threads created by grpc::Server will inherit a copy of the signal mask.
    ::sigemptyset(&blocked_signals);
    ::sigaddset(&blocked_signals, SIGHUP);
    ::sigaddset(&blocked_signals, SIGINT);
    ::sigaddset(&blocked_signals, SIGQUIT);
    ::sigaddset(&blocked_signals, SIGTERM);
    const auto pthread_sigmask_result = ::pthread_sigmask(SIG_BLOCK, &blocked_signals, nullptr);
    constexpr int operation_success = 0;
    if (pthread_sigmask_result != operation_success)
    {
        throw std::runtime_error{std::strerror(pthread_sigmask_result)};
    }

    SearchLoggerHistoryService search_logger_history_service;
    LoggerService logger_service;
    auto diagnostics_service =
            LibDiagnostics::make_grpc_service(
                    Fred::Logger::Diagnostics::about,
                    Fred::Logger::Diagnostics::status);

    grpc::ServerBuilder builder;

    // Listen on the given address without any authentication mechanism.
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

    builder.RegisterService(&logger_service);
    builder.RegisterService(&search_logger_history_service);
    builder.RegisterService(diagnostics_service.get());

    // Finally assemble the server.
    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    LIBLOG_INFO("listen on " + server_address);

    int received_signal;
    const auto sigwait_result = ::sigwait(&blocked_signals, &received_signal);
    if (sigwait_result != operation_success)
    {
        throw std::runtime_error{std::strerror(sigwait_result)};
    }
    switch (received_signal)
    {
        case SIGHUP:
            LIBLOG_DEBUG("SIGHUP received");
            break;
        case SIGINT:
            LIBLOG_DEBUG("SIGINT received");
            break;
        case SIGQUIT:
            LIBLOG_DEBUG("SIGQUIT received");
            break;
        case SIGTERM:
            LIBLOG_DEBUG("SIGTERM received");
            break;
        default:
            throw std::runtime_error{"unexpected signal received"};
    }
    const std::chrono::system_clock::time_point deadline = std::chrono::system_clock::now() + std::chrono::seconds(10);
    server->Shutdown(deadline);
}

} // namespace Fred::Logger::{anonymous}
} // namespace Fred::Logger
} // namespace Fred

int main(int argc, char *argv[])
{
    try
    {
        Cfg::Options::init(argc, argv);
    }
    catch (const Cfg::AllDone& all_done)
    {
        std::cout << all_done.what() << std::endl;
        return EXIT_SUCCESS;
    }
    catch (const Cfg::UnknownOption& unknown_option)
    {
        std::cerr << unknown_option.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const Cfg::MissingOption& missing_option)
    {
        std::cerr << missing_option.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const Cfg::Exception& e)
    {
        std::cerr << "Configuration problem: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        std::cerr << "Unexpected exception caught" << std::endl;
        return EXIT_FAILURE;
    }

    constexpr LibLog::ThreadMode threading = LibLog::ThreadMode::multi_threaded; // global, compile-time parameter
    std::unique_ptr<Fred::Logger::LibPgLogging> libpq_log_layer;
    auto liblog_log_config = LibLog::LogConfig{};
    switch (boost::apply_visitor(Fred::Logger::SetLogHandler{liblog_log_config}, Cfg::Options::get().log.device))
    {
        case Fred::Logger::LogHandler::is_set:
            LibLog::Log::start<threading>(liblog_log_config);
            libpq_log_layer = std::make_unique<Fred::Logger::LibPgLogging>();
            LIBLOG_DEBUG("fred-logger-services configuration successfully loaded");
            break;
        case Fred::Logger::LogHandler::is_not_set:
            std::cout << "fred-logger-services configuration successfully loaded" << std::endl;
            break;
    }

    try
    {
        Fred::Logger::run_server(Cfg::Options::get().logger.listen_on);
        LIBLOG_INFO("Server shutdown");
        return EXIT_SUCCESS;
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        std::cerr << "Unexpected exception caught" << std::endl;
        return EXIT_FAILURE;
    }
}
