/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_OBJECT_REFERENCE_TYPES_HH_4B4105BDAD704D739C22510B5D6D2184
#define GET_OBJECT_REFERENCE_TYPES_HH_4B4105BDAD704D739C22510B5D6D2184

#include "liblogger/log_entry.hh"

#include <vector>

namespace Fred {
namespace Logger {

struct GetObjectReferenceTypesRequest
{
};

} // namespace Fred::Logger
} // namespace Fred

#endif
