/*
 * Copyright (C) 2020-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CLOSE_LOG_ENTRY_HH_B677ABC309854BFCB1B5FDEF018B9E93
#define CLOSE_LOG_ENTRY_HH_B677ABC309854BFCB1B5FDEF018B9E93

#include "liblogger/log_entry.hh"
#include "liblogger/log_entry_ident.hh"
#include "liblogger/session_ident.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <vector>

namespace Fred {
namespace Logger {

struct CloseLogEntryRequest
{
    LibLogger::LogEntryIdent log_entry_ident;
    LibStrong::Optional<LibLogger::LogEntry::Content> log_entry_content;
    std::vector<LibLogger::LogEntryProperty> log_entry_properties;
    std::vector<LibLogger::ObjectReferences> object_references;
    boost::variant<LibLogger::LogEntry::ResultCode, LibLogger::LogEntry::ResultName> result_code_or_name;
    boost::optional<LibLogger::SessionIdent> session_ident;
};

} // namespace Fred::Logger
} // namespace Fred

#endif
