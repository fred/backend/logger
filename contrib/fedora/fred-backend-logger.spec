Name:           %{project_name}
Version:        %{our_version}
Release:        %{?our_release}%{!?our_release:1}%{?dist}
Summary:	FRED - GRPC logger backend
Group:		Applications/Utils
License:	GPLv3+
URL:		http://fred.nic.cz
Source:         %{name}-%{version}.tar.gz
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  findutils cmake pkgconf make gcc-c++ mpdecimal-devel libssh-devel libidn-devel openssl-devel boost-devel grpc-devel grpc-plugins protobuf-devel protobuf-compiler postgresql-devel systemd
Requires:       libssh libidn openssl boost grpc protobuf libpq

%description
FRED (Free Registry for Enum and Domain) is free registry system for 
managing domain registrations. This package contains GRPC logger backend.

%prep
%setup

%build
%cmake .
%cmake_build

%install
rm -rf $RPM_BUILD_ROOT
%cmake_install
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/fred/
install fred-logger-services.conf.example $RPM_BUILD_ROOT/%{_sysconfdir}/fred/fred-logger-services.conf
mkdir -p $RPM_BUILD_ROOT/%{_unitdir}
install -m 644 contrib/fedora/fred-backend-logger.service $RPM_BUILD_ROOT/%{_unitdir}

%pre
/usr/bin/getent passwd fred || /usr/sbin/useradd -r -d /etc/fred -s /bin/bash fred

%post
test -f /var/log/fred-logger-services.log  || touch /var/log/fred-logger-services.log
chown fred /var/log/fred-logger-services.log

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%config %{_sysconfdir}/fred/fred-logger-services.conf
%{_sbindir}/fred-logger-services
%{_unitdir}/fred-backend-logger.service
