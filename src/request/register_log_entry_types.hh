/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REGISTER_LOG_ENTRY_TYPES_HH_2B8F878FE8E4458596C7837531D46D44
#define REGISTER_LOG_ENTRY_TYPES_HH_2B8F878FE8E4458596C7837531D46D44

#include "liblogger/log_entry.hh"

#include <vector>

namespace Fred {
namespace Logger {

struct RegisterLogEntryTypesRequest
{
    LibLogger::LogEntry::ServiceName service;
    std::vector<LibLogger::LogEntry::LogEntryTypeName> log_entry_types;
};

} // namespace Fred::Logger
} // namespace Fred

#endif
