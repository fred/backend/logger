/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EXCEPTIONS_HH_6B5FD4A49DFC4A01B1221033C4129D50
#define EXCEPTIONS_HH_6B5FD4A49DFC4A01B1221033C4129D50

#include <exception>

namespace Fred {
namespace Logger {

struct RequiredArgumentMissing : std::exception
{
    const char* what() const noexcept override;
};

struct InvalidIpAddress : std::exception
{
    const char* what() const noexcept override;
};

} // namespace Fred::Logger
} // namespace Fred

#endif
