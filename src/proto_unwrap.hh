/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PROTO_UNWRAP_HH_A1CE82340F4A42558C763705118AA11B
#define PROTO_UNWRAP_HH_A1CE82340F4A42558C763705118AA11B

#include "request/register_log_entry_types.hh"
#include "request/register_object_reference_types.hh"
#include "request/register_results.hh"
#include "request/register_service.hh"
#include "request/close_log_entry.hh"
#include "request/close_session.hh"
#include "request/create_log_entry.hh"
#include "request/create_session.hh"
#include "request/get_log_entry_info.hh"
#include "request/get_log_entry_types.hh"
#include "request/get_object_reference_types.hh"
#include "request/get_results.hh"
#include "request/get_services.hh"
#include "request/list_log_entries.hh"

#include "fred_api/logger/service_logger_grpc.pb.h"
#include "fred_api/logger/service_search_logger_history_grpc.pb.h"

namespace Fred {
namespace Logger {

CreateLogEntryRequest proto_unwrap(const Api::CreateLogEntryRequest& _request);

CloseLogEntryRequest proto_unwrap(const Api::CloseLogEntryRequest& _request);

CreateSessionRequest proto_unwrap(const Api::CreateSessionRequest& _request);

CloseSessionRequest proto_unwrap(const Api::CloseSessionRequest& _request);

GetLogEntryTypesRequest proto_unwrap(const Api::GetLogEntryTypesRequest& _request);

GetResultsRequest proto_unwrap(const Api::GetResultsRequest& _request);

RegisterServiceRequest proto_unwrap(const Api::RegisterServiceRequest& _request);

RegisterResultsRequest proto_unwrap(const Api::RegisterResultsRequest& _request);

RegisterLogEntryTypesRequest proto_unwrap(const Api::RegisterLogEntryTypesRequest& _request);

RegisterObjectReferenceTypesRequest proto_unwrap(const Api::RegisterObjectReferenceTypesRequest& _request);

ListLogEntriesRequest proto_unwrap(const Api::ListLogEntriesRequest& _request);

GetLogEntryInfoRequest proto_unwrap(const Api::GetLogEntryInfoRequest& _src);

} // namespace Fred::Logger
} // namespace Fred

#endif
