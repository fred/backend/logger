ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

* Changed register_service implementation so that the database sequence
  is not always incremented if service already exists (liblogger)

1.6.1 (2024-02-22)
------------------

* Return service name in get_log_entry_info as is (not in lowercase)

1.6.0 (2023-10-04)
------------------

* List log entries also by properties
* Syslog facility changed from 0..7 to LOG_LOCALx (x=0..7)
* Better handling of monitoring host file in configuration


1.5.1 (2022-04-08)
------------------

* Fix new log entry id format
* Fix fedora copr build (dependencies fetching)


1.5.0 (2022-03-25)
------------------

* Integrate common diagnostics service using ``libdiagnostics``
* Adapt to ``libstrong`` library
* Simplify composed log entry id format (implementation detail)
* Simplify composed session id format (implementation detail)
* Changed ``LogEntryInfo``

  * add ``log_entry_id``
  * add ``session_id``

* Update CI
* RPM build


1.4.0 (2020-08-26)
------------------

* Add functions to register new *service*, *log entry type*, *result* and *object reference type*


1.3.0 (2020-05-17)
------------------

* Use ``liblogger`` - core functions moved to a newly established library


1.2.0 (2020-06-09)
------------------

* Changed ``get_log_entry_info`` - separate input/output log entry properties


1.1.0 (2020-04-22)
------------------

* Initial ``Logger`` implementation
* Rewrite ``SearchLoggerHistory`` to use ``libpg`` and ``liblog``


1.0.0 (2019-08-13)
------------------

* Initial ``SearchLoggerHistory`` implementation
