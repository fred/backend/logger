/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/diagnostics/status.hh"

#include "src/get_dsn.hh"

#include "libpg/pg_rw_transaction.hh"

#include "fred_api/logger/service_logger_grpc.grpc.pb.h"
#include "fred_api/logger/service_search_logger_history_grpc.grpc.pb.h"

namespace Fred {
namespace Logger {
namespace Diagnostics {

LibDiagnostics::StatusReply status()
{

    const auto query = "SELECT 'status'::TEXT WHERE false";
    bool db_accessible = false;
    try
    {
        LibPg::PgRwTransaction rw_transaction{get_dsn()};
        exec(rw_transaction, query);
        commit(std::move(rw_transaction));
        db_accessible = true;
    }
    catch (...)
    {
    }

    const auto services = {
            Api::Logger::service_full_name(),
            Api::SearchLoggerHistory::service_full_name()};

    LibDiagnostics::StatusReply reply;

    if (db_accessible)
    {
        const auto status_ok =
                LibDiagnostics::StatusReply::Service{
                        LibDiagnostics::StatusReply::Status::ok,
                        LibDiagnostics::StatusReply::Note{"OK"},
                        std::map<LibDiagnostics::StatusReply::ExtraKey, LibDiagnostics::StatusReply::ExtraValue>{}};
        for (const auto& service : services)
        {
            reply.services[service] = status_ok;
        }
    }
    else
    {
        const auto status_error =
                LibDiagnostics::StatusReply::Service{
                        LibDiagnostics::StatusReply::Status::error,
                        LibDiagnostics::StatusReply::Note{"Database not accessible."},
                        std::map<LibDiagnostics::StatusReply::ExtraKey, LibDiagnostics::StatusReply::ExtraValue>{}};
        for (const auto& service : services)
        {
            reply.services[service] = status_error;
        }
    }
    return reply;
}

} // namespace Fred::Logger::Diagnostics
} // namespace Fred::Logger
} // namespace Fred
